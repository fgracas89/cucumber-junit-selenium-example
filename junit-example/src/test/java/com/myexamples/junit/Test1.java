package com.myexamples.junit;

import com.myexamples.selenium.BrowserFactory;
import com.myexamples.selenium.SeleniumHelper;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class Test1 {

    private WebDriver browser;

    @Test
    public void test_something_with_success(){
        assertTrue(true);
    }


    @Test
    public void test_something_with_errors(){
        fail();
    }


    @Test
    public void searchXrayOnGoogle() throws IOException, InterruptedException {
        browser = BrowserFactory.initializeBrowser(BrowserFactory.Browser.FIREFOX);
        browser.get("https://www.google.com");
        browser.findElement(By.id("lst-ib")).sendKeys("Xray Jira");
        Thread.sleep(1000);
        SeleniumHelper.takeScreenshot(browser, "target/surefire-reports");
        browser.findElement(By.id("lst-ib")).sendKeys(Keys.ENTER);
        Thread.sleep(1000);
        SeleniumHelper.takeScreenshot(browser, "target/surefire-reports");
        browser.quit();
    }


    @After
    public void tearDown(){
        //we cannot leave instances of the browser opened
        if(browser != null){
            browser.quit();
        }
    }
}
