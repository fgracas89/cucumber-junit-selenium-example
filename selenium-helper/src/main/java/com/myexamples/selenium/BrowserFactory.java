package com.myexamples.selenium;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class BrowserFactory {

    public enum Browser {
        CHROME, FIREFOX
    }

    private static int WIDTH = 1920;
    private static int HEIGHT = 1080;


    public static WebDriver initializeBrowser(Browser browserChoice){

        WebDriver browser = null;

        switch (browserChoice){
            case CHROME: browser = initializeChrome(); break;
            case FIREFOX: browser = initializeFirefox(); break;
        }

        assert browser != null;
        resizeBrowser(browser);
        return browser;
    }


    private static WebDriver initializeFirefox(){
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(true);
        return new FirefoxDriver(firefoxOptions);
    }

    private static WebDriver initializeChrome(){
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(true);
        return new ChromeDriver(chromeOptions);
    }


    private static void resizeBrowser(WebDriver browser){
        browser.manage().window().setSize(new Dimension(WIDTH, HEIGHT));
    }


}
