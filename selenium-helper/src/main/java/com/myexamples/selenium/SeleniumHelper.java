package com.myexamples.selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.function.Function;


public class SeleniumHelper {

    public static WebElement getParentElement(WebElement element){
        return element.findElement(By.xpath("parent::*"));
    }

    public static String getElementHTML(WebElement element){
        return element.getAttribute("outerHTML");
    }

    public static void waitUntil(WebDriver browser, int timeout, Function<WebDriver, Boolean> condition){
        WebDriverWait wait = new WebDriverWait(browser, timeout);
        wait.until(condition);
    }

    public static byte[] takeScreenshot(WebDriver browser){
        return ((TakesScreenshot) browser).getScreenshotAs(OutputType.BYTES);
    }

    public static Path takeScreenshot(WebDriver browser, String folder) throws IOException {
        File temporaryFile = ((TakesScreenshot) browser).getScreenshotAs(OutputType.FILE);
        Path destinationFolder = Paths.get(folder);
        if(!Files.exists(destinationFolder, LinkOption.NOFOLLOW_LINKS)) Files.createDirectory(destinationFolder);
        String fileName = "screenshot-" + System.currentTimeMillis() + ".png";
        return Files.copy(temporaryFile.toPath(), Paths.get(folder, fileName), StandardCopyOption.REPLACE_EXISTING);
    }

}
