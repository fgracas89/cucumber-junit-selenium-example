package com.myexamples.cucumber.step_definitions;

import com.myexamples.cucumber.lib.PageManager;
import com.myexamples.cucumber.lib.page_objects.GoogleResultsPage;
import com.myexamples.cucumber.lib.page_objects.GoogleSearchPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.lang.reflect.InvocationTargetException;

public class GoogleSearchTest {

    @Given("^I open my browser and i access Google$")
    public void iOpenMyBrowserAndIAccessGoogle() {
        try {
            PageManager.getInstance().startUpPage(GoogleSearchPage.class);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            System.err.println("UUUUPSSS");
            e.printStackTrace();
        }
    }

    @When("^I search \"([^\"]*)\"$")
    public void iSearch(String searchKeywords) throws InterruptedException {
        GoogleResultsPage nextPage = ((GoogleSearchPage)PageManager.getInstance().getCurrentPage()).search(searchKeywords);
        PageManager.getInstance().transitionPage(nextPage);
    }

    @Then("^I should have an result for a site \"([^\"]*)\"$")
    public void iShouldHaveAnResultForASite(String siteUrl) {
        ((GoogleResultsPage)PageManager.getInstance().getCurrentPage()).checkForResult(siteUrl);
    }

    @Then("^I should have (\\d+) search results$")
    public void i_should_have_search_results(int numberOfResults) throws Exception {
        ((GoogleResultsPage)PageManager.getInstance().getCurrentPage()).checkNumberOfSearchResults(numberOfResults);
    }
}
