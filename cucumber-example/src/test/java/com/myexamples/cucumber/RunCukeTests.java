package com.myexamples.cucumber;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

//DEBUG
//mvn -Dmaven.surefire.debug clean test -P cucumber -Dcucumber.options="--tags @TEST1"
//for debugging with different port than 5005
//mvn -Dmaven.surefire.debug="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000 -Xnoagent -Djava.compiler=NONE" clean test -P cucumber -Dcucumber.options="--tags @TEST1"
//NORMAL
//mvn clean test -Dcucumber.options="--tags @TEST1"
@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
//        features="src/test/com/myexamples/cucumber",
        plugin = {
            "pretty",
            "html:target/public",
            "rerun:target/public/rerun.txt",
            "junit:target/public/cucumber-junit-report.xml",
            "json:target/public/cucumber-report.json"
        }
)
public class RunCukeTests {
}