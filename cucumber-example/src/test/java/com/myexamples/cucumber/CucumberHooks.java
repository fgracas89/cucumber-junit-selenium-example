package com.myexamples.cucumber;

import com.myexamples.cucumber.lib.PageManager;
import com.myexamples.selenium.SeleniumHelper;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.openqa.selenium.WebDriver;

public class CucumberHooks {


    @After
    public void afterScenarioHook(Scenario scenario){

        WebDriver browser = PageManager.getInstance().getBrowser();

        //append screenshot if test fails
        if (scenario.isFailed() && browser != null) {
            scenario.embed(SeleniumHelper.takeScreenshot(browser), "image/png");
        }

        //close browser
        if(browser != null){
            browser.quit();
        }

        //reset state of PageManager
        PageManager.getInstance().clear();
    }

}
