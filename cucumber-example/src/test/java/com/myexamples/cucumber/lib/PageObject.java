package com.myexamples.cucumber.lib;

import org.openqa.selenium.WebDriver;

public abstract class PageObject {

    protected WebDriver browser;

    public PageObject(WebDriver browser){ this.browser = browser; }

    public WebDriver getBrowser() { return browser; }


}
