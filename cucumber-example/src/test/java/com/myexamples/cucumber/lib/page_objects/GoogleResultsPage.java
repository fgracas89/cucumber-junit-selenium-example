package com.myexamples.cucumber.lib.page_objects;

import com.myexamples.cucumber.lib.PageObject;
import org.openqa.selenium.WebDriver;

public class GoogleResultsPage extends PageObject {

    public GoogleResultsPage(WebDriver browser) {
        super(browser);
        System.out.println("Initializing GoogleResultsPage");
    }

    public void checkNumberOfSearchResults(int numberOfResults) throws Exception {
        if(numberOfResults >= 0 && numberOfResults <= 1){
            throw new Exception("Your argument is invalid! Google has always more than 1 results");
        }
    }


    public void checkForResult(String siteUrl) {
        //do nothing here, just a demo
    }
}
