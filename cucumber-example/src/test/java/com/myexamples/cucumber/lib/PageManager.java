package com.myexamples.cucumber.lib;

import com.myexamples.selenium.BrowserFactory;
import org.openqa.selenium.WebDriver;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class PageManager {

    private static PageManager ourInstance = new PageManager();
    public static PageManager getInstance() {
        return ourInstance;
    }
    private PageManager() {}

    private PageObject currentPage = null;

    public PageObject getCurrentPage() {
        return currentPage;
    }

    public WebDriver getBrowser(){
        return currentPage.getBrowser();
    }

    public <T extends PageObject> void startUpPage(Class<T> pageObjectClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<T> constructor = pageObjectClass.getConstructor(WebDriver.class);

        WebDriver browser;
        String browserChoice = System.getenv("TEST_BROWSER");
        if(browserChoice != null) {
            browserChoice = browserChoice.toLowerCase();
            switch (browserChoice){
                case "firefox": browser = BrowserFactory.initializeBrowser(BrowserFactory.Browser.FIREFOX); break;
                case "chrome": browser = BrowserFactory.initializeBrowser(BrowserFactory.Browser.CHROME); break;
                default: browser = BrowserFactory.initializeBrowser(BrowserFactory.Browser.FIREFOX); break;
            }
        }else{
            browser = BrowserFactory.initializeBrowser(BrowserFactory.Browser.FIREFOX);
        }

        currentPage = constructor.newInstance(browser);
    }

    public void transitionPage(PageObject newPage){
        currentPage = newPage;
    }

    public void clear(){
        currentPage = null;
    }
}
