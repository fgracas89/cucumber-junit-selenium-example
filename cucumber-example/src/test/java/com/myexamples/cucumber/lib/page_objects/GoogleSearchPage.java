package com.myexamples.cucumber.lib.page_objects;

import com.myexamples.cucumber.lib.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GoogleSearchPage extends PageObject {

    public GoogleSearchPage(WebDriver browser) {
        super(browser);
        System.out.println("Initializing GoogleResultsPage");
    }


    public GoogleResultsPage search(String query) throws InterruptedException {
        browser.get("https://www.google.com");
        browser.findElement(By.id("lst-ib")).sendKeys(query);
        Thread.sleep(1000);
        return new GoogleResultsPage(browser);
    }

}
