@TX-9
Feature: I want to search on google


  @TX-11 @TX-12 @release-1 @xray
  Scenario: Xray results on Google
    Given I open my browser and i access Google
    When I search "Xray Jira"
    Then I should have 1 search results


  @TX-10 @TX-12 @release-1 @sunny-day @xray
  Scenario: Search Xray on Google
    Given I open my browser and i access Google
    When I search "Xray Jira"
    Then I should have an result for a site "www.xpandit.com"


  @TX-20 @TX-12 @release-1 @xray
  Scenario Outline: Test with multiple queries (Scenario Outline)
    Given I open my browser and i access Google
    When I search "<Query>"
    Then I should have <Number of Results> search results
    Examples:
      | Query  | Number of Results |
      | Jira   | 10                |
      | Wiki   | 10                |
      | Bamboo | 1                 |