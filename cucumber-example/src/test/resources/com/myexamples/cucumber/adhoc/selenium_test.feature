# Created by fgracas at 4/19/18
Feature: Selenium usage example
  # Enter feature description here

  Scenario: Search Zephyr bamboo on Google
    # Enter steps here
    Given I open my browser and i access Google
    When I search "Zephyr Jira"
    Then I should have an result for a site "Zephyr"


  Scenario: Failed Zephyr search on Google
    # Enter steps here
    Given I open my browser and i access Google
    When I search "Zephyr Bamboo"
    Then I should have 0 search results